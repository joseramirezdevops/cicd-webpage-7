from flask import Flask
from flask_frozen import Freezer

# 1. Instantiate a class
app = Flask(__name__) #Create a Flask application instance with the name "app"
freezer = Freezer(app) # Create a Freezer Instance with the name "freezer"

# 2. Define Paths for Web pages using flask-frozen
app.config['FREEZER_BASE_URL'] = 'https://joseramirezdevops.gitlab.io' # Set up URL
app.config['FREEZER_DESTINATION'] = 'public' # Set folder to paste files

# 3. Define Custom functions
@app.cli.command("renderCustomCommand") # To define custom command "renderCustomCommand" . Command Line Interface in a composable way with as little code as necessary
def renderCustomCommand(): #custom command definiton to be able to upload files to public folder in the Website
    freezer.freeze()

# 4. Path Web requests/responses
@app.route('/')
def displayMessage():
    return "Hello. Today is Tuesday. June  22, 2021."
